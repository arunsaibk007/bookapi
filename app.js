const express = require('express');
const bodyParser = require('body-parser');

const { mongoose } = require('./db/mongoose');
const { Book } = require('./models/book');
const { User } = require('./models/user');

const port = process.env.PORT || 3000;
const app = express();

//middleware to log
app.use((req, res, next) => {
    console.log(`User logged at ${new Date().toString()} : ${req.method} ${req.url} `);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization, x-auth');
    res.setHeader('Access-Control-Expose-Headers', 'x-auth');
    next();
});

app.use(bodyParser.json());

bookRouter = require('./routes/bookRoutes')(Book);
app.use('/api/books', bookRouter); 

app.get('/', (req, res) => {
    res.send('server is up and running!')
});

app.listen(port, () => {
    console.log(`server is up and running on poort ${port}`);
});