let mongoose = require('mongoose');

let User = mongoose.model('User', {
    _id: {
        type: Number,
        required: true,
        unique: true
    },
    name : {
        type: String,
        required : true,
        minlength : 1,
        trim : true
    }
});

module.exports = { User }