let mongoose = require('mongoose');

let Book = mongoose.model('Book', {
    _id: {
        type: Number,
        required: true,
        unique: true
    },
    title : {
        type: String,
        required : true,
        minlength : 1,
        trim : true
    },
    price : {
        type: Number,
        default : 0
    },   
    rating: {
        type: Number,
        default : 0
    },
    users : [{
        _id: {
            type: Number,
            required: true
        }        
    }]
});

module.exports = { Book }