
var bookController = (Book) => {
    var post = (req, res) => {
        var book = new Book(req.body);

        if (!req.body.title) {
            res.status(400);
            res.send('Title is required');
        }
        else {
            book.save();
            res.status(201);
            res.send(book);
        }
    }

    var get = (req, res) => {
       Book.find((err, books) => {
            if (err)
                res.status(500).send(err);
            else {
                res.json(books);
            }
        });
    }

    return {
        post,
        get
    }
}

module.exports =  bookController 