let mongoose = require('mongoose');

mongoose.Promise = global.Promise;
process.env.MONGODB_URI  = 'mongodb://testuser:test123@ds125628.mlab.com:25628/bookapiapp';
mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/BookApiApp');

module.exports = {
    mongoose
}